﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class OrdersLine
    {
        public int Id;
        public int BookID;
        public int OrderID;
        public float Price;
        public int Amount;
    }
}
