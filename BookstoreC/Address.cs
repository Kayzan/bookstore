﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class Address
    {
        public int Id;
        public int ClientID;      
        public string AddressLine1;
        public string AddressLine2;
        public string ZipCode;
        public DateTime Deleted;
    }
}
