﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class Author
    {
        public int Id;
        public string FirstName;
        public string LastName;
        public DateTime BirthDate;
        public DateTime DeathDate;
    }
}
