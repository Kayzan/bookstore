﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class OrdersRepository
    {
        public List<Orders> GetAllOrders()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreC.Properties.Settings.BookstoreConnectionString"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var Sql = $"Select * From Orders";
                var command = new SqlCommand(Sql, connection);
                var ordersList = new List<Orders>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var orders = new Orders();
                            orders.Id = Convert.ToInt32(reader["Id"]);
                            orders.ClientID = Convert.ToInt32(reader["ClientID"]);
                            orders.BillingAddressID = Convert.ToInt32(reader["BillingAddressID"]);
                            orders.ShippingAddressID = Convert.ToInt32(reader["ShippingAddressID"]);
                            orders.OrderDate = Convert.ToDateTime(reader["OrderDate"]);
                            orders.OrderStatus = reader["OrderStatus"].ToString();
                            ordersList.Add(orders);
                        }
                    }
                }
                return ordersList;
            }
        }
    }
}
