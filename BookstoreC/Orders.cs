﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class Orders
    {
        public int Id;
        public int ClientID;
        public int BillingAddressID;
        public int ShippingAddressID;
        public DateTime OrderDate;
        public string OrderStatus;
    }
}
