﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class AuthorRepository
    {
        public List<Author> GetAllAuthor()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreC.Properties.Settings.BookstoreConnectionString"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var Sql = $"Select * From Author";
                var command = new SqlCommand(Sql, connection);
                var authorList = new List<Author>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var author = new Author();
                            author.Id = Convert.ToInt32(reader["Id"]);
                            author.FirstName = reader["FirstName"].ToString();
                            author.LastName = reader["LastName"].ToString();
                            author.BirthDate = Convert.ToDateTime(reader["BirthDate"]);
                            author.DeathDate = Convert.ToDateTime(reader["DeathDate"]);
                            authorList.Add(author);
                        }
                    }
                }
                return authorList;
            }
        }
    }
}
