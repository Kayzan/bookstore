﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class OrdersLineRepository
    {
        public List<OrdersLine> GetAllOrdersLine()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreC.Properties.Settings.BookstoreConnectionString"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var Sql = $"Select * From OrdersLine";
                var command = new SqlCommand(Sql, connection);
                var orderslineList = new List<OrdersLine>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var ordersline = new OrdersLine();
                            ordersline.Id = Convert.ToInt32(reader["Id"]);
                            ordersline.BookID = Convert.ToInt32(reader["BookID"]);
                            ordersline.OrderID = Convert.ToInt32(reader["OrderID"]);
                            ordersline.Price = Convert.ToInt32(reader["Price"]);
                            ordersline.Amount = Convert.ToInt32(reader["Amount"]);
                            orderslineList.Add(ordersline);
                        }
                    }
                }
                return orderslineList;
            }
        }
    }
}
