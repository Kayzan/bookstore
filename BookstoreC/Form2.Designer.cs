﻿namespace BookstoreC
{
    partial class FrmInsert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnAddAuthor = new System.Windows.Forms.Button();
            this.TxtAuthorID = new System.Windows.Forms.TextBox();
            this.TxtAuthorName = new System.Windows.Forms.TextBox();
            this.TxtAuthorLast = new System.Windows.Forms.TextBox();
            this.TxtAuthorBirth = new System.Windows.Forms.TextBox();
            this.TxtAuthorDeath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnAddClient = new System.Windows.Forms.Button();
            this.TxtClientID = new System.Windows.Forms.TextBox();
            this.TxtClientFirstName = new System.Windows.Forms.TextBox();
            this.TxtClientLastName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnAddClientAddress = new System.Windows.Forms.Button();
            this.TxtClientAddressID = new System.Windows.Forms.TextBox();
            this.TxtClientCity = new System.Windows.Forms.TextBox();
            this.TxtClientStreet = new System.Windows.Forms.TextBox();
            this.TxtAddressLine2 = new System.Windows.Forms.TextBox();
            this.TxtZippCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnAddAuthor
            // 
            this.BtnAddAuthor.Location = new System.Drawing.Point(284, 31);
            this.BtnAddAuthor.Name = "BtnAddAuthor";
            this.BtnAddAuthor.Size = new System.Drawing.Size(152, 45);
            this.BtnAddAuthor.TabIndex = 0;
            this.BtnAddAuthor.Text = "Dodawanie nowego autora";
            this.BtnAddAuthor.UseVisualStyleBackColor = true;
            this.BtnAddAuthor.Click += new System.EventHandler(this.BtnAddAuthor_Click);
            // 
            // TxtAuthorID
            // 
            this.TxtAuthorID.Location = new System.Drawing.Point(12, 92);
            this.TxtAuthorID.Name = "TxtAuthorID";
            this.TxtAuthorID.Size = new System.Drawing.Size(130, 20);
            this.TxtAuthorID.TabIndex = 1;
            // 
            // TxtAuthorName
            // 
            this.TxtAuthorName.Location = new System.Drawing.Point(148, 92);
            this.TxtAuthorName.Name = "TxtAuthorName";
            this.TxtAuthorName.Size = new System.Drawing.Size(130, 20);
            this.TxtAuthorName.TabIndex = 2;
            // 
            // TxtAuthorLast
            // 
            this.TxtAuthorLast.Location = new System.Drawing.Point(284, 92);
            this.TxtAuthorLast.Name = "TxtAuthorLast";
            this.TxtAuthorLast.Size = new System.Drawing.Size(130, 20);
            this.TxtAuthorLast.TabIndex = 3;
            // 
            // TxtAuthorBirth
            // 
            this.TxtAuthorBirth.Location = new System.Drawing.Point(420, 92);
            this.TxtAuthorBirth.Name = "TxtAuthorBirth";
            this.TxtAuthorBirth.Size = new System.Drawing.Size(130, 20);
            this.TxtAuthorBirth.TabIndex = 4;
            // 
            // TxtAuthorDeath
            // 
            this.TxtAuthorDeath.Location = new System.Drawing.Point(556, 92);
            this.TxtAuthorDeath.Name = "TxtAuthorDeath";
            this.TxtAuthorDeath.Size = new System.Drawing.Size(130, 20);
            this.TxtAuthorDeath.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(145, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Imię";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nazwisko";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(417, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Data urodzenia";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(553, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Data śmierci";
            // 
            // BtnAddClient
            // 
            this.BtnAddClient.Location = new System.Drawing.Point(284, 176);
            this.BtnAddClient.Name = "BtnAddClient";
            this.BtnAddClient.Size = new System.Drawing.Size(151, 38);
            this.BtnAddClient.TabIndex = 11;
            this.BtnAddClient.Text = "Dodawanie nowego klienta";
            this.BtnAddClient.UseVisualStyleBackColor = true;
            this.BtnAddClient.Click += new System.EventHandler(this.BtnAddClient_Click);
            // 
            // TxtClientID
            // 
            this.TxtClientID.Location = new System.Drawing.Point(12, 253);
            this.TxtClientID.Name = "TxtClientID";
            this.TxtClientID.Size = new System.Drawing.Size(129, 20);
            this.TxtClientID.TabIndex = 12;
            // 
            // TxtClientFirstName
            // 
            this.TxtClientFirstName.Location = new System.Drawing.Point(285, 253);
            this.TxtClientFirstName.Name = "TxtClientFirstName";
            this.TxtClientFirstName.Size = new System.Drawing.Size(129, 20);
            this.TxtClientFirstName.TabIndex = 13;
            // 
            // TxtClientLastName
            // 
            this.TxtClientLastName.Location = new System.Drawing.Point(557, 253);
            this.TxtClientLastName.Name = "TxtClientLastName";
            this.TxtClientLastName.Size = new System.Drawing.Size(129, 20);
            this.TxtClientLastName.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "ID";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(282, 290);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Imię";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(554, 301);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Nazwisko";
            // 
            // BtnAddClientAddress
            // 
            this.BtnAddClientAddress.Location = new System.Drawing.Point(284, 329);
            this.BtnAddClientAddress.Name = "BtnAddClientAddress";
            this.BtnAddClientAddress.Size = new System.Drawing.Size(151, 35);
            this.BtnAddClientAddress.TabIndex = 18;
            this.BtnAddClientAddress.Text = "Dodawanie adresu klienta";
            this.BtnAddClientAddress.UseVisualStyleBackColor = true;
            this.BtnAddClientAddress.Click += new System.EventHandler(this.BtnAddClientAddress_Click);
            // 
            // TxtClientAddressID
            // 
            this.TxtClientAddressID.Location = new System.Drawing.Point(12, 414);
            this.TxtClientAddressID.Name = "TxtClientAddressID";
            this.TxtClientAddressID.Size = new System.Drawing.Size(128, 20);
            this.TxtClientAddressID.TabIndex = 19;
            // 
            // TxtClientCity
            // 
            this.TxtClientCity.Location = new System.Drawing.Point(150, 414);
            this.TxtClientCity.Name = "TxtClientCity";
            this.TxtClientCity.Size = new System.Drawing.Size(128, 20);
            this.TxtClientCity.TabIndex = 20;
            // 
            // TxtClientStreet
            // 
            this.TxtClientStreet.Location = new System.Drawing.Point(286, 414);
            this.TxtClientStreet.Name = "TxtClientStreet";
            this.TxtClientStreet.Size = new System.Drawing.Size(128, 20);
            this.TxtClientStreet.TabIndex = 21;
            // 
            // TxtAddressLine2
            // 
            this.TxtAddressLine2.Location = new System.Drawing.Point(422, 414);
            this.TxtAddressLine2.Name = "TxtAddressLine2";
            this.TxtAddressLine2.Size = new System.Drawing.Size(128, 20);
            this.TxtAddressLine2.TabIndex = 22;
            // 
            // TxtZippCode
            // 
            this.TxtZippCode.Location = new System.Drawing.Point(556, 414);
            this.TxtZippCode.Name = "TxtZippCode";
            this.TxtZippCode.Size = new System.Drawing.Size(128, 20);
            this.TxtZippCode.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 452);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "ID Klienta";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(147, 452);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Miasto";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(283, 452);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Ulica";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(419, 452);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Informacje dodatkowe";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(554, 452);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Kod pocztowy";
            // 
            // FrmInsert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 483);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtZippCode);
            this.Controls.Add(this.TxtAddressLine2);
            this.Controls.Add(this.TxtClientStreet);
            this.Controls.Add(this.TxtClientCity);
            this.Controls.Add(this.TxtClientAddressID);
            this.Controls.Add(this.BtnAddClientAddress);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtClientLastName);
            this.Controls.Add(this.TxtClientFirstName);
            this.Controls.Add(this.TxtClientID);
            this.Controls.Add(this.BtnAddClient);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtAuthorDeath);
            this.Controls.Add(this.TxtAuthorBirth);
            this.Controls.Add(this.TxtAuthorLast);
            this.Controls.Add(this.TxtAuthorName);
            this.Controls.Add(this.TxtAuthorID);
            this.Controls.Add(this.BtnAddAuthor);
            this.Name = "FrmInsert";
            this.Text = "Dodawanie danych do tabeli";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnAddAuthor;
        private System.Windows.Forms.TextBox TxtAuthorID;
        private System.Windows.Forms.TextBox TxtAuthorName;
        private System.Windows.Forms.TextBox TxtAuthorLast;
        private System.Windows.Forms.TextBox TxtAuthorBirth;
        private System.Windows.Forms.TextBox TxtAuthorDeath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnAddClient;
        private System.Windows.Forms.TextBox TxtClientID;
        private System.Windows.Forms.TextBox TxtClientFirstName;
        private System.Windows.Forms.TextBox TxtClientLastName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BtnAddClientAddress;
        private System.Windows.Forms.TextBox TxtClientAddressID;
        private System.Windows.Forms.TextBox TxtClientCity;
        private System.Windows.Forms.TextBox TxtClientStreet;
        private System.Windows.Forms.TextBox TxtAddressLine2;
        private System.Windows.Forms.TextBox TxtZippCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}