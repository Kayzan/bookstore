﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class AddressRepository
    {
        public List<Address> GetAllAddress()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreC.Properties.Settings.BookstoreConnectionString"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var Sql = $"Select * From Address";
                var command = new SqlCommand(Sql, connection);
                var addressList = new List<Address>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {                      
                        while (reader.Read())
                        {
                            var address = new Address();
                            address.Id = Convert.ToInt32(reader["Id"]);
                            address.ClientID = Convert.ToInt32(reader["ClientID"]);
                            address.AddressLine1 = reader["AddresLine1"].ToString();
                            address.ZipCode = reader["ZipCode"].ToString();
                            addressList.Add(address);
                        }                                         
                    }
                }
                return addressList;
            }
        }


    }
}
