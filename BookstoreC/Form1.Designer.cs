﻿namespace BookstoreC
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnGetAddress = new System.Windows.Forms.Button();
            this.LstAddress = new System.Windows.Forms.ListBox();
            this.BtnShowBooks = new System.Windows.Forms.Button();
            this.LstBooks = new System.Windows.Forms.ListBox();
            this.LstClients = new System.Windows.Forms.ListBox();
            this.BtnShowClients = new System.Windows.Forms.Button();
            this.BtnShowOrders = new System.Windows.Forms.Button();
            this.LstOrders = new System.Windows.Forms.ListBox();
            this.BtnShowOrdersLine = new System.Windows.Forms.Button();
            this.LstOrdersLine = new System.Windows.Forms.ListBox();
            this.BtnInsertForm = new System.Windows.Forms.Button();
            this.BtnShowAuthor = new System.Windows.Forms.Button();
            this.LstShowAuthor = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // BtnGetAddress
            // 
            this.BtnGetAddress.Location = new System.Drawing.Point(12, 141);
            this.BtnGetAddress.Name = "BtnGetAddress";
            this.BtnGetAddress.Size = new System.Drawing.Size(157, 49);
            this.BtnGetAddress.TabIndex = 0;
            this.BtnGetAddress.Text = "Pokaż adresy klientów księgarni";
            this.BtnGetAddress.UseVisualStyleBackColor = true;
            this.BtnGetAddress.Click += new System.EventHandler(this.BtnGetAddress_Click);
            // 
            // LstAddress
            // 
            this.LstAddress.FormattingEnabled = true;
            this.LstAddress.Location = new System.Drawing.Point(14, 196);
            this.LstAddress.Name = "LstAddress";
            this.LstAddress.Size = new System.Drawing.Size(755, 69);
            this.LstAddress.TabIndex = 2;
            // 
            // BtnShowBooks
            // 
            this.BtnShowBooks.Location = new System.Drawing.Point(12, 271);
            this.BtnShowBooks.Name = "BtnShowBooks";
            this.BtnShowBooks.Size = new System.Drawing.Size(156, 59);
            this.BtnShowBooks.TabIndex = 3;
            this.BtnShowBooks.Text = "Pokaż dostępne książki";
            this.BtnShowBooks.UseVisualStyleBackColor = true;
            this.BtnShowBooks.Click += new System.EventHandler(this.BtnShowBooks_Click);
            // 
            // LstBooks
            // 
            this.LstBooks.FormattingEnabled = true;
            this.LstBooks.Location = new System.Drawing.Point(11, 336);
            this.LstBooks.Name = "LstBooks";
            this.LstBooks.Size = new System.Drawing.Size(758, 69);
            this.LstBooks.TabIndex = 4;
            // 
            // LstClients
            // 
            this.LstClients.FormattingEnabled = true;
            this.LstClients.Location = new System.Drawing.Point(12, 66);
            this.LstClients.Name = "LstClients";
            this.LstClients.Size = new System.Drawing.Size(757, 69);
            this.LstClients.TabIndex = 5;
            // 
            // BtnShowClients
            // 
            this.BtnShowClients.Location = new System.Drawing.Point(12, 12);
            this.BtnShowClients.Name = "BtnShowClients";
            this.BtnShowClients.Size = new System.Drawing.Size(154, 48);
            this.BtnShowClients.TabIndex = 6;
            this.BtnShowClients.Text = "Pokaż wszystkich klientów księgarni";
            this.BtnShowClients.UseVisualStyleBackColor = true;
            this.BtnShowClients.Click += new System.EventHandler(this.BtnShowClients_Click);
            // 
            // BtnShowOrders
            // 
            this.BtnShowOrders.Location = new System.Drawing.Point(11, 422);
            this.BtnShowOrders.Name = "BtnShowOrders";
            this.BtnShowOrders.Size = new System.Drawing.Size(154, 48);
            this.BtnShowOrders.TabIndex = 7;
            this.BtnShowOrders.Text = "Pokaż wszystkie zamówienia";
            this.BtnShowOrders.UseVisualStyleBackColor = true;
            this.BtnShowOrders.Click += new System.EventHandler(this.BtnShowOrders_Click);
            // 
            // LstOrders
            // 
            this.LstOrders.FormattingEnabled = true;
            this.LstOrders.Location = new System.Drawing.Point(11, 476);
            this.LstOrders.Name = "LstOrders";
            this.LstOrders.Size = new System.Drawing.Size(758, 69);
            this.LstOrders.TabIndex = 8;
            // 
            // BtnShowOrdersLine
            // 
            this.BtnShowOrdersLine.Location = new System.Drawing.Point(11, 551);
            this.BtnShowOrdersLine.Name = "BtnShowOrdersLine";
            this.BtnShowOrdersLine.Size = new System.Drawing.Size(157, 49);
            this.BtnShowOrdersLine.TabIndex = 9;
            this.BtnShowOrdersLine.Text = "Pokaż linię zamówień";
            this.BtnShowOrdersLine.UseVisualStyleBackColor = true;
            this.BtnShowOrdersLine.Click += new System.EventHandler(this.BtnShowOrdersLine_Click);
            // 
            // LstOrdersLine
            // 
            this.LstOrdersLine.FormattingEnabled = true;
            this.LstOrdersLine.Location = new System.Drawing.Point(11, 606);
            this.LstOrdersLine.Name = "LstOrdersLine";
            this.LstOrdersLine.Size = new System.Drawing.Size(758, 82);
            this.LstOrdersLine.TabIndex = 10;
            // 
            // BtnInsertForm
            // 
            this.BtnInsertForm.Location = new System.Drawing.Point(312, 912);
            this.BtnInsertForm.Name = "BtnInsertForm";
            this.BtnInsertForm.Size = new System.Drawing.Size(162, 38);
            this.BtnInsertForm.TabIndex = 11;
            this.BtnInsertForm.Text = "Przejście do dodawania danych";
            this.BtnInsertForm.UseVisualStyleBackColor = true;
            this.BtnInsertForm.Click += new System.EventHandler(this.BtnInsertForm_Click);
            // 
            // BtnShowAuthor
            // 
            this.BtnShowAuthor.Location = new System.Drawing.Point(15, 714);
            this.BtnShowAuthor.Name = "BtnShowAuthor";
            this.BtnShowAuthor.Size = new System.Drawing.Size(182, 45);
            this.BtnShowAuthor.TabIndex = 12;
            this.BtnShowAuthor.Text = "Pokaż wszystkich autorów";
            this.BtnShowAuthor.UseVisualStyleBackColor = true;
            this.BtnShowAuthor.Click += new System.EventHandler(this.BtnShowAuthor_Click);
            // 
            // LstShowAuthor
            // 
            this.LstShowAuthor.FormattingEnabled = true;
            this.LstShowAuthor.Location = new System.Drawing.Point(11, 789);
            this.LstShowAuthor.Name = "LstShowAuthor";
            this.LstShowAuthor.Size = new System.Drawing.Size(780, 82);
            this.LstShowAuthor.TabIndex = 13;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 977);
            this.Controls.Add(this.LstShowAuthor);
            this.Controls.Add(this.BtnShowAuthor);
            this.Controls.Add(this.BtnInsertForm);
            this.Controls.Add(this.LstOrdersLine);
            this.Controls.Add(this.BtnShowOrdersLine);
            this.Controls.Add(this.LstOrders);
            this.Controls.Add(this.BtnShowOrders);
            this.Controls.Add(this.BtnShowClients);
            this.Controls.Add(this.LstClients);
            this.Controls.Add(this.LstBooks);
            this.Controls.Add(this.BtnShowBooks);
            this.Controls.Add(this.LstAddress);
            this.Controls.Add(this.BtnGetAddress);
            this.Name = "FrmMenu";
            this.Text = "Bookstore";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnGetAddress;
        private System.Windows.Forms.ListBox LstAddress;
        private System.Windows.Forms.Button BtnShowBooks;
        private System.Windows.Forms.ListBox LstBooks;
        private System.Windows.Forms.ListBox LstClients;
        private System.Windows.Forms.Button BtnShowClients;
        private System.Windows.Forms.Button BtnShowOrders;
        private System.Windows.Forms.ListBox LstOrders;
        private System.Windows.Forms.Button BtnShowOrdersLine;
        private System.Windows.Forms.ListBox LstOrdersLine;
        private System.Windows.Forms.Button BtnInsertForm;
        private System.Windows.Forms.Button BtnShowAuthor;
        private System.Windows.Forms.ListBox LstShowAuthor;
    }
}

