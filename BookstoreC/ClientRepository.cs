﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class ClientRepository
    {              
        public List<Client> GetAllClient()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreC.Properties.Settings.BookstoreConnectionString"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var Sql = $"Select * From Client";
                var command = new SqlCommand(Sql, connection);
                var clientList = new List<Client>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var client = new Client();
                            client.Id = Convert.ToInt32(reader["Id"]);
                            client.FirstName = reader["FirstName"].ToString();
                            client.LastName = reader["LastName"].ToString();
                            clientList.Add(client);
                        }
                    }
                }
                return clientList;
            }
        }


    }
}
