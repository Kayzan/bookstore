﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookstoreC
{


    public partial class FrmInsert : Form
    {


        public FrmInsert()
        {
            InitializeComponent();

            if (Debugger.IsAttached)
                CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo("en-US");
        }

        SqlCommand cmd;
        SqlConnection con;
        SqlDataAdapter da;

        public void InsertData()
        {


           /* string conString = ConfigurationManager.ConnectionStrings["BookstoreC.Properties.Settings.BookstoreConnectionString"].ConnectionString;
            SqlConnection Con = new SqlConnection(conString);


            SqlCommand Cmd = new SqlCommand("INSERT INTO Author " +
        "(FirstName, LastName, BirthDate, DeathDate) " +
                "VALUES(@FirstName, @LastName, @BirthDate, @DeathDate)",
        Con);


            //Cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
            Cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar);
            Cmd.Parameters.Add("@LastName", System.Data.SqlDbType.VarChar);
            Cmd.Parameters.Add("@BirthDate", System.Data.SqlDbType.VarChar);
            Cmd.Parameters.Add("@DeathDate", System.Data.SqlDbType.DateTime);



            //Cmd.Parameters["@Id"].Value = Convert.ToInt32(TxtAuthorID.Text);
            Cmd.Parameters["@FirstName"].Value = TxtAuthorName.Text;
            Cmd.Parameters["@LastName"].Value = TxtAuthorLast.Text;
            Cmd.Parameters["@BirthDate"].Value = DateTime.Parse(TxtAuthorBirth.Text);
            Cmd.Parameters["@DeathDate"].Value = DateTime.Parse(TxtAuthorDeath.Text);

            Con.Open();


            int RowsAffected = Cmd.ExecuteNonQuery();


            Con.Close();*/

        }

        private void BtnAddAuthor_Click(object sender, EventArgs e)
        {
            {
                InsertData();
                
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Michal\source\repos\BookstoreC\BookstoreC\Bookstore.mdf;Integrated Security=True");
                con.Open();
                cmd = new SqlCommand("INSERT INTO Author (FirstName, LastName, BirthDate, DeathDate) VALUES (@FirstName, @LastName, @BirthDate, @DeathDate)", con);

                cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar);
                cmd.Parameters.Add("@LastName", System.Data.SqlDbType.VarChar);
                cmd.Parameters.Add("@BirthDate", System.Data.SqlDbType.DateTime);
                cmd.Parameters.Add("@DeathDate", System.Data.SqlDbType.DateTime);

                cmd.Parameters["@FirstName"].Value = TxtAuthorName.Text;
                cmd.Parameters["@LastName"].Value = TxtAuthorLast.Text;
                cmd.Parameters["@BirthDate"].Value = DateTime.Parse(TxtAuthorBirth.Text);
                cmd.Parameters["@DeathDate"].Value = DateTime.Parse(TxtAuthorDeath.Text);
                cmd.ExecuteNonQuery();

                con.Close();
            }

        }

        private void BtnAddClient_Click(object sender, EventArgs e)
        {
            con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Michal\source\repos\BookstoreC\BookstoreC\Bookstore.mdf;Integrated Security=True");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Client (FirstName, LastName) VALUES (@FirstName, @LastName)", con);
          
            cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar);
            cmd.Parameters.Add("@LastName", System.Data.SqlDbType.VarChar);

            cmd.Parameters["@FirstName"].Value = TxtClientFirstName.Text;
            cmd.Parameters["@LastName"].Value = TxtClientLastName.Text;

            cmd.ExecuteNonQuery();
            con.Close();
        }

        private void BtnAddClientAddress_Click(object sender, EventArgs e)
        {
            
            con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Michal\source\repos\BookstoreC\BookstoreC\Bookstore.mdf;Integrated Security=True");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Address (ClientID, City, AddresLine1, AddressLine2, ZipCode) VALUES (@ClientID, @City, @AddresLine1, @AddressLine2, @ZipCode)", con);

            cmd.Parameters.Add("@ClientID", System.Data.SqlDbType.Int);
            cmd.Parameters.Add("@City", System.Data.SqlDbType.VarChar);
            cmd.Parameters.Add("@AddresLine1", System.Data.SqlDbType.VarChar );
            cmd.Parameters.Add("@AddressLine2", System.Data.SqlDbType.VarChar);
            cmd.Parameters.Add("@ZipCode", System.Data.SqlDbType.VarChar);

            cmd.Parameters["@ClientID"].Value = TxtClientAddressID.Text;           
            cmd.Parameters["@City"].Value = TxtClientCity.Text;
            cmd.Parameters["@AddresLine1"].Value = TxtClientStreet.Text;
            cmd.Parameters["@AddressLine2"].Value = TxtAddressLine2.Text;
            cmd.Parameters["@ZipCode"].Value = (TxtZippCode.Text);

            cmd.ExecuteNonQuery();
            con.Close();
        }
    }

}
