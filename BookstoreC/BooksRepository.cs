﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class BookRepository
    {        
        public List<Book> GetAllBook()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreC.Properties.Settings.BookstoreConnectionString"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var Sql = $"Select * From Book";
                var command = new SqlCommand(Sql, connection);
                var bookList = new List<Book>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var book = new Book();
                            book.Id = Convert.ToInt32(reader["Id"]);
                            book.AuthorID = Convert.ToInt32(reader["AuthorID"]);
                            book.Title = reader["Title"].ToString();
                            book.Date = Convert.ToDateTime(reader["Date"]);
                            book.Price = Convert.ToInt32(reader["Price"]);
                            bookList.Add(book);                        
                        }
                    }
                }
                return bookList;
            }
        }
    }
}
