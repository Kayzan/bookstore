﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookstoreC
{
    class Book
    {
        public int Id;
        public int AuthorID;
        public string Title;
        public DateTime Date;
        public float Price;
        public DateTime Deleted;
    }
}
