﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookstoreC
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        private void BtnGetAddress_Click(object sender, EventArgs e)
        {
            var repository = new AddressRepository();                    
            var alladdress = repository.GetAllAddress();
            foreach (var singleaddress in alladdress)
            {
                LstAddress.Items.Add($"Id Klienta:  {singleaddress.ClientID},  Adres: {singleaddress.AddressLine1}  Kod pocztowy: {singleaddress.ZipCode}");
            }
        }

        private void BtnShowBooks_Click(object sender, EventArgs e)
        {
            var repository = new BookRepository();            
            var allbooks = repository.GetAllBook();
            foreach (var singlebook in allbooks)
            {
                LstBooks.Items.Add($"Id: {singlebook.Id}, ID Autora: {singlebook.AuthorID}, Tytuł: {singlebook.Title}, Data wydania: {singlebook.Date}, Cena: {singlebook.Price}");
            }
        }

        private void BtnShowClients_Click(object sender, EventArgs e)
        {
            var repository = new ClientRepository();         
            var allclients = repository.GetAllClient();
            foreach (var singleclient in allclients)
            {
                LstClients.Items.Add($"Id klienta: {singleclient.Id}, Imię klienta: {singleclient.FirstName}, Nazwisko klienta: {singleclient.LastName}");
            }
        }

        private void BtnShowOrders_Click(object sender, EventArgs e)
        {
            var repository = new OrdersRepository();
            var allorders = repository.GetAllOrders();
            foreach (var singleorder in allorders)
            {
                LstOrders.Items.Add($"Id zamówienia: {singleorder.Id}, Id Klienta: {singleorder.ClientID}, ID adresu do faktury: {singleorder.BillingAddressID}, ID adresu dostawy: {singleorder.ShippingAddressID}, Data zamówienia: {singleorder.OrderDate}, Status zamówienia: {singleorder.OrderStatus} ");
            }

        }

        private void BtnShowOrdersLine_Click(object sender, EventArgs e)
        {
            var repository = new OrdersLineRepository();
            var allordersline = repository.GetAllOrdersLine();
            foreach (var singleordersline in allordersline)
            {
                LstOrdersLine.Items.Add($"Id linii zamówienia: {singleordersline.Id}, ID Książki: {singleordersline.BookID}, Id zamówienia: {singleordersline.OrderID}, Cena: {singleordersline.Price}, Ilość: {singleordersline.Amount}");
            }
        }

        private void BtnInsertForm_Click(object sender, EventArgs e)
        {
            FrmInsert P1 = new FrmInsert();
            P1.Show();
        }

        private void BtnShowAuthor_Click(object sender, EventArgs e)
        {
            var repository = new AuthorRepository();
            var allauthor = repository.GetAllAuthor();
            foreach (var singleauthor in allauthor)
            {
                LstShowAuthor.Items.Add($"Id autora: {singleauthor.Id}, Imię autora: {singleauthor.FirstName}, Nazwisko autora: {singleauthor.LastName}, Data urodzenia: {singleauthor.BirthDate}, Data śmierci: {singleauthor.DeathDate}");
            }
        }
    }
}
